import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import configReducer from '../component/config-reducer';
import travelReducer from '../component/travel/travel-reducer';
import travelSaga from '../component/travel/travel-saga';

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: {
    travel: travelReducer,
    config: configReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ thunk: false }).concat(sagaMiddleware),
});

sagaMiddleware.run(travelSaga);

export default store;
