import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setIsMobile, setIsPortrait } from '../config-reducer';

const useCheckBrowserWidth = () => {
  const dispatch = useDispatch();
  const handleWindowSizeChange = () => {
    dispatch(setIsMobile(/iPhone|iPod|Android/i.test(navigator.userAgent)));
    dispatch(
      setIsPortrait(
        window.innerWidth < window.innerHeight || window.innerWidth < 720
      )
    );
  };

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange);
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange);
    };
  });

  useEffect(() => {
    handleWindowSizeChange();
  });

  return <></>;
};

export default useCheckBrowserWidth;
