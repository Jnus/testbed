import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface IAppReducerProps {
  isMobile: boolean;
  isPortrait: boolean;
}

const configSlice = createSlice({
  name: 'config',
  initialState: {
    isMobile: false,
    isPortrait: false,
  },
  reducers: {
    setIsMobile: (state: IAppReducerProps, action: PayloadAction<boolean>) => {
      state.isMobile = action.payload;
    },
    setIsPortrait: (
      state: IAppReducerProps,
      action: PayloadAction<boolean>
    ) => {
      state.isPortrait = action.payload;
    },
  },
});

export const { setIsMobile, setIsPortrait } = configSlice.actions;

export default configSlice.reducer;
