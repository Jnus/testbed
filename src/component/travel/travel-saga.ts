import { call, put, takeLatest } from 'redux-saga/effects';
import { getTrips } from './travel-api';
import { PayloadAction } from '@reduxjs/toolkit';
import { setError, setTrips } from './travel-reducer';
import { ITrip } from './travel-type';

function* loadTrips(action: PayloadAction<string[]>) {
  if (action.payload.length < 1) {
    return;
  }

  try {
    const trips: ITrip[] = yield call(getTrips, action.payload[0]);
    yield put(setTrips(trips));
  } catch (e) {
    yield put(setError(e.response.status));
  }
}

export const sagaActions = {
  LOAD_TRIPS: 'LOAD_TRIPS',
};

function* travelSaga() {
  yield takeLatest(sagaActions.LOAD_TRIPS, loadTrips);
}

export default travelSaga;
