import axios from 'axios';

export const getTrips = async (country: string) => {
  try {
    const res = await axios.get(
      `https://raw.githubusercontent.com/JnusK/content/main/travel/${country.toLowerCase()}/trips.json`
    );
    return res.data;
  } catch (error) {
    throw error;
  }
};

//Promise.then does not seem to wait for the response, might be wrongly written
