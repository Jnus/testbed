import { ITravelState } from './travel-type';
import { createSelector } from '@reduxjs/toolkit';

const selectPath = (state: ITravelState) => {
  return state.path;
};

export const getPath = createSelector([selectPath], (path) => path);
