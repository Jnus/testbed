export interface ITravelState {
  error: string;
  path: string[];
  trips: ITrip[];
  content: IContent | {};
}

export interface ITrip {
  title: string;
  byline: string;
  date: string;
  content: string[];
  thumbnail: string;
}

export interface IContent {
  date: string;
  title: string;
  text: string[];
  images?: string[];
  imageIndex?: number[];
}
