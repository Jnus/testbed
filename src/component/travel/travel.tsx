import React, { useEffect, useState } from 'react';
import Map from './map/map';
import ReactTooltip from 'react-tooltip';
import Destination from './destination/destination';
import { connect, useDispatch } from 'react-redux';
import { changePath } from './travel-reducer';
import { sagaActions } from './travel-saga';
import { ITrip } from './travel-type';

import styles from './travel.module.css';
import TravelError from './error/travel-error';

interface ITravelProps {
  changePath: (path: string[]) => void;
  error: string;
  path: string[];
  trips: ITrip[];
}

const Travel = (props: ITravelProps) => {
  const [tooltipContent, setTooltipContent] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    if (props.path.length === 1) {
      dispatch({ type: sagaActions.LOAD_TRIPS, payload: props.path });
    }
  }, [props.path, dispatch]);

  return (
    <>
      <h1 className={styles.moduleTitle}>Travel</h1>
      {props.path && props.path.length > 0 ? (
        <Destination
          path={props.path}
          setPath={(path: string[]) => props.changePath(path)}
          trips={props.trips}
        />
      ) : (
        <>
          <Map
            setPath={(path: string[]) => props.changePath(path)}
            setTooltipContent={setTooltipContent}
          />
          <ReactTooltip>{tooltipContent}</ReactTooltip>
        </>
      )}
      {props.error !== '' && <TravelError error={props.error} />}
    </>
  );
};

const mapStateToProps = (state: any) => ({
  error: state.travel.error,
  path: state.travel.path,
  trips: state.travel.trips,
});

const mapDispatchToProps = { changePath };

export default connect(mapStateToProps, mapDispatchToProps)(Travel);
