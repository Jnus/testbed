import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ITravelState, ITrip } from './travel-type';

const travelSlice = createSlice({
  name: 'travel',
  initialState: {
    error: '',
    path: [],
    trips: [],
    content: {},
  } as ITravelState,
  reducers: {
    changePath: (state: ITravelState, action: PayloadAction<string[]>) => {
      state.error = '';
      state.path = [...action.payload];
    },
    resetPath: (state: ITravelState) => {
      state.error = '';
      state.path = [];
      state.trips = [];
    },
    setError: (state: ITravelState, action: PayloadAction<number>) => {
      if (action.payload === 404) {
        state.error = `Trips to ${state.path[0]} are still en route. They will need a while to arrive. Please select some other country.`;
      } else {
        state.error = `There has been some trouble fetching the data. Please try again later.`;
      }
    },
    setTrips: (state: ITravelState, action: PayloadAction<ITrip[]>) => {
      state.error = '';
      state.trips = action.payload;
    },
  },
});

export const {
  changePath,
  resetPath,
  setError,
  setTrips,
} = travelSlice.actions;

export default travelSlice.reducer;
