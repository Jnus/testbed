import axios from 'axios';
import { getTrips } from './travel-api';

jest.mock('axios');

describe('Travel API', () => {
  test('fetch data successfully', async () => {
    const data = {
      data: [
        {
          date: '2020-01-01',
          title: 'Test Trip',
          byline: 'Byline',
          content: ['test 1', 'test 2'],
          thumbnail: 'img',
        },
      ],
    };

    // @ts-ignore
    axios.get.mockImplementationOnce(() => Promise.resolve(data));

    expect(await getTrips('Test')).toEqual(data.data);

    expect(axios.get).toHaveBeenCalledWith(
      'https://raw.githubusercontent.com/JnusK/content/main/travel/test/trips.json'
    );
  });

  test('Error should be thrown', async () => {
    const errorMessage = 'Request failed with status code 404';
    // @ts-ignore
    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );

    await expect(getTrips('Test')).rejects.toThrow(errorMessage);
  });
});
