import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import Destination from '../destination/destination';
import { ITrip } from '../travel-type';
import SelectTrip from './select-trip';
import TripContent from './trip-content';

const mockTrips: ITrip[] = [
  {
    date: '2020-02',
    title: 'Test Title',
    byline: 'Test Byline',
    content: ['Test Content'],
    thumbnail:
      'https://raw.githubusercontent.com/JnusK/content/main/travel/fuji-thumbnail.jpg',
  },
  {
    date: '2020-02',
    title: 'React Title',
    byline: 'React Byline',
    content: ['React Content'],
    thumbnail:
      'https://raw.githubusercontent.com/JnusK/content/main/travel/fuji-thumbnail.jpg',
  },
];

test('Breadcrumb click will set path', () => {
  const setPath = jest.fn();
  render(
    <Destination
      path={['Japan', 'Fuji-san']}
      setPath={setPath}
      trips={mockTrips}
    />
  );

  fireEvent.click(screen.getByText('Japan'));

  expect(setPath).toBeCalledWith(['Japan']);
});

test('Select Trip will show map of trips in cards', () => {
  const setTrip = jest.fn();
  render(<SelectTrip setTrip={setTrip} trips={mockTrips} />);

  expect(screen.getByText('Test Title')).toBeVisible();
  expect(screen.getByText('React Byline')).toBeVisible();
});

test('Select Trip click on card will set trip', () => {
  const setTrip = jest.fn();

  render(<SelectTrip setTrip={setTrip} trips={mockTrips} />);

  fireEvent.click(screen.getByText('React Title'));

  expect(setTrip).toHaveBeenCalledWith(1);
});

test('Trip Content renders correctly', () => {
  render(
    <TripContent
      trip={{
        date: '2020-02',
        title: 'React Title',
        byline: 'React Byline',
        content: ['React Content', 'paragraph 2'],
        thumbnail:
          'https://raw.githubusercontent.com/JnusK/content/main/travel/fuji-thumbnail.jpg',
      }}
    />
  );

  expect(screen.getByText('React Title')).toBeVisible();
  expect(screen.getByText('paragraph 2')).toBeVisible();
});
