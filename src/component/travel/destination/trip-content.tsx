import React from 'react';
import { ITrip } from '../travel-type';

import styles from './destination.module.css';

interface ITripContentProps {
  trip: ITrip;
}

const TripContent = (props: ITripContentProps) => {
  const img = new RegExp('https://w*');

  return (
    <div className={styles.contentContainer}>
      <div>{props.trip.title}</div>
      {props.trip.content.map((line: string, index) => {
        if (line.match(img)) {
          return (
            <img
              className={styles.contentImage}
              src={line}
              alt={line}
              key={index}
            />
          );
        }
        return (
          <div className={styles.contentText} key={index}>
            {line}
          </div>
        );
      })}
    </div>
  );
};

export default TripContent;
