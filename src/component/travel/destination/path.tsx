import React from 'react';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { Breadcrumbs } from '@material-ui/core';

import styles from './destination.module.css';

interface IPathProps {
  path: string[];
  setPath: (path: string[]) => void;
}

const Path: React.FC<IPathProps> = ({ path, setPath }) => {
  return (
    <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />}>
      <div className={styles.breadcrumb} onClick={() => setPath([])}>
        Map
      </div>
      {path &&
        path.map((segment, index) => {
          if (path.length - index === 1) {
            return (
              <div className={styles.breadcrumbActive} key={index}>
                {segment}
              </div>
            );
          }
          return (
            <div
              className={styles.breadcrumb}
              key={index}
              onClick={() => setPath(path.slice(0, index + 1))}
            >
              {segment}
            </div>
          );
        })}
    </Breadcrumbs>
  );
};

export default Path;
