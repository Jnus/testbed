import React, { useState } from 'react';

import './destination.module.css';
import Path from './path';
import TripContent from './trip-content';
import SelectTrip from './select-trip';
import { ITrip } from '../travel-type';

import styles from './destination.module.css';

interface IDestinationProps {
  path: string[];
  setPath: (country: string[]) => void;
  trips: ITrip[];
}

const Destination = (props: IDestinationProps) => {
  const [currentTrip, setCurrentTrip] = useState(-1);

  const setTrip = (index: number) => {
    setCurrentTrip(index);
    props.setPath([props.path[0], props.trips[index].title]);
  };

  const setPath = (path: string[]) => {
    setCurrentTrip(-1);
    props.setPath(path);
  };

  return (
    <>
      <div className={styles.destinationContainer}>
        <Path path={props.path} setPath={setPath} />
        <h2 className={styles.containerTitle}>
          {props.path[props.path.length - 1]}
        </h2>
        {currentTrip >= 0 ? (
          <TripContent trip={props.trips[currentTrip] || []} />
        ) : (
          <SelectTrip setTrip={setTrip} trips={props.trips} />
        )}
      </div>
    </>
  );
};

export default Destination;
