import React, { Dispatch, SetStateAction } from 'react';
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
} from '@material-ui/core';
import { ITrip } from '../travel-type';

import styles from './destination.module.css';

interface ISelectTripProps {
  setTrip: Dispatch<SetStateAction<any>>;
  trips: ITrip[];
}

const SelectTrip = (props: ISelectTripProps) => {
  const handleClick = (
    event: React.MouseEvent<HTMLAnchorElement | HTMLButtonElement>,
    index: number
  ) => {
    props.setTrip(index);
  };

  return (
    <Grid container spacing={2}>
      {props.trips?.map((trip, index) => (
        <Grid item key={index}>
          <Card className={styles.card}>
            <CardActionArea onClick={(event) => handleClick(event, index)}>
              <CardMedia
                className={styles.cardImage}
                image={trip.thumbnail}
                title={trip.title}
              />
              <CardContent>
                <h3>{trip.title}</h3>
                <div>{trip.byline}</div>
                <div>{trip.date}</div>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export default SelectTrip;
