import React from 'react';
import errorImg from '../../../static/img/trip-not-found.png';

import styles from '../travel.module.css';

interface ITravelErrorProps {
  error: string;
}

const TravelError: React.FC<ITravelErrorProps> = ({ error }) => {
  return (
    <>
      <img src={errorImg} className={styles.errorImg} alt="Error message" />
      <div>{error}</div>
    </>
  );
};

export default TravelError;
