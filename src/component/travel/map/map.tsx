import React, { memo } from 'react';
import {
  ZoomableGroup,
  ComposableMap,
  Geographies,
  Geography,
} from 'react-simple-maps';

import map from '../../../static/map.json';

interface IMapProps {
  setPath: (value: string[]) => void;
  setTooltipContent: (content: string) => void;
}

const Map = (props: IMapProps) => {
  return (
    <>
      <ComposableMap
        data-tip=""
        // height={1000}
        projectionConfig={{ scale: 200 }}
        style={{ width: '80%' }}
        width={1200}
      >
        <ZoomableGroup>
          <Geographies geography={map}>
            {({ geographies }) =>
              geographies.map((geo) => (
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  onMouseEnter={() => {
                    const { NAME } = geo.properties;
                    props.setTooltipContent(`${NAME}`);
                  }}
                  onMouseLeave={() => {
                    props.setTooltipContent('');
                  }}
                  onClick={() => {
                    if (geo.properties.TRAVEL) {
                      props.setPath([geo.properties.NAME]);
                    }
                  }}
                  style={{
                    default: {
                      fill: geo.properties.TRAVEL ? '#ABBCFF' : '#D6D6DA',
                      outline: 'none',
                    },
                    hover: {
                      cursor: geo.properties.TRAVEL ? 'pointer' : 'default',
                      fill: geo.properties.TRAVEL ? '#0a3bff' : '#D6D6DA',
                      outline: 'none',
                    },
                    pressed: {
                      fill: '#E42',
                      outline: 'none',
                    },
                  }}
                />
              ))
            }
          </Geographies>
        </ZoomableGroup>
      </ComposableMap>
    </>
  );
};

export default memo(Map);
