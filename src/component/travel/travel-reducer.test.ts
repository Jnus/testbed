import travelSlice, {
  changePath,
  resetPath,
  setError,
  setTrips,
} from './travel-reducer';

describe('Travel Reducer', () => {
  test('Set error should set correct error if 404 response', () => {
    const initialState = {
      error: '',
      path: ['Test'],
      trips: [],
      content: {},
    };
    expect(
      travelSlice(initialState, {
        type: setError.type,
        payload: 404,
      })
    ).toEqual({
      content: {},
      error:
        'Trips to Test are still en route. They will need a while to arrive. Please select some other country.',
      path: ['Test'],
      trips: [],
    });
  });

  test('Set error should set correct error if 500 response', () => {
    const initialState = {
      error: '',
      path: ['Test'],
      trips: [],
      content: {},
    };
    expect(
      travelSlice(initialState, {
        type: setError.type,
        payload: 500,
      })
    ).toEqual({
      content: {},
      error:
        'There has been some trouble fetching the data. Please try again later.',
      path: ['Test'],
      trips: [],
    });
  });

  test('Reset path should reset the state to initial', () => {
    const initialState = {
      error: '',
      path: ['Test'],
      trips: [],
      content: {},
    };
    expect(
      travelSlice(initialState, {
        type: resetPath.type,
      })
    ).toEqual({
      content: {},
      error: '',
      path: [],
      trips: [],
    });
  });

  test('Change path should change path to state', () => {
    const initialState = {
      error: 'Error',
      path: [],
      trips: [],
      content: {},
    };

    const newPath = ['Test', 'Meh'];

    expect(
      travelSlice(initialState, {
        type: changePath.type,
        payload: newPath,
      })
    ).toEqual({
      error: '',
      path: newPath,
      trips: [],
      content: {},
    });
  });

  test('Set trips should change path to state', () => {
    const initialState = {
      error: 'Error',
      path: ['Test'],
      trips: [],
      content: {},
    };

    const newTrips = ['Test', 'Meh'];

    expect(
      travelSlice(initialState, {
        type: setTrips.type,
        payload: newTrips,
      })
    ).toEqual({
      ...initialState,
      trips: newTrips,
      error: '',
    });
  });
});
