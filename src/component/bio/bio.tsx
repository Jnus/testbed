import React from 'react';
import { connect } from 'react-redux';
import janusImg from '../../static/img/januskiong.jpg';

import styles from './bio.module.css';

interface IBioProps {
  isMobile: boolean;
  isPortrait: boolean;
}

const Bio: React.FC<IBioProps> = ({ isMobile, isPortrait }) => {
  return (
    <div>
      <div className={styles.moduleHeader}>Bio</div>
      <div
        className={
          isPortrait ? styles.bioContainerPortrait : styles.bioContainer
        }
      >
        <div>
          <p>Bio Text</p>
          <p>I will always find time to travel and cook.</p>
        </div>
        <div>
          <img alt="Janus Kiong" className={styles.bioImg} src={janusImg} />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  isMobile: state.config.isMobile,
  isPortrait: state.config.isPortrait,
});

export default connect(mapStateToProps, {})(Bio);
