import Footer from './footer';
import { render, screen } from '@testing-library/react';

describe('Footer', () => {
  test('should display correctly', () => {
    window.open = jest.fn();
    render(<Footer />);
    expect(screen.getByText('Created by Janus Kiong')).toBeVisible();
    screen.getByTestId('linkedInIcon').click();
    expect(window.open).toHaveBeenCalledWith(
      'https://www.linkedin.com/in/januskiong/',
      '_blank'
    );
    screen.getByTestId('gitlabIcon').click();
    expect(window.open).toHaveBeenCalledWith(
      'https://www.linkedin.com/in/januskiong/',
      '_blank'
    );
  });
});
