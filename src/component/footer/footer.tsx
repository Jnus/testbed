import React from 'react';
import { Icon, IconButton } from '@material-ui/core';
import styles from './footer.module.css';
import { faGitlab, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Footer = () => {
  const clickLinkedIn = () => {
    window.open('https://www.linkedin.com/in/januskiong/', '_blank');
  };

  const clickGitlab = () => {
    window.open('https://gitlab.com/Jnus', '_blank');
  };

  return (
    <div className={styles.footerContainer}>
      <div className={styles.subscript}>Created by Janus Kiong</div>
      <div>
        <IconButton onClick={clickLinkedIn} data-testid="linkedInIcon">
          <Icon className={styles.iconContainer}>
            <FontAwesomeIcon icon={faLinkedinIn} />
          </Icon>
        </IconButton>
        <IconButton onClick={clickGitlab} data-testid="gitlabIcon">
          <Icon className={styles.iconContainer}>
            <FontAwesomeIcon icon={faGitlab} />
          </Icon>
        </IconButton>
      </div>
    </div>
  );
};

export default Footer;
