import React, { useState } from 'react';
import {
  Icon,
  IconButton,
  List,
  ListItem,
  ListItemText,
  SwipeableDrawer,
} from '@material-ui/core';

import styles from './header.module.css';
import { IHeaderProps } from './header-type';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

const SidebarHeader: React.FC<IHeaderProps> = ({
  currentPage,
  changePage,
  menu,
  name,
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleDrawer = () => {
    setIsOpen(!isOpen);
  };

  const onClick = (page: string) => {
    changePage(page);
    toggleDrawer();
  };

  return (
    <>
      <div className={styles.sidebarHeader}>
        <div className={styles.buttonContainer} />
        <div className={styles.headerName}>{name}</div>
        <IconButton data-testid="sidebar-button" onClick={() => toggleDrawer()}>
          <Icon className={styles.iconContainer}>
            <FontAwesomeIcon icon={faBars} />
          </Icon>
        </IconButton>
      </div>
      <SwipeableDrawer
        anchor="right"
        onClose={toggleDrawer}
        onOpen={toggleDrawer}
        open={isOpen}
      >
        <List>
          {menu.map((item: string) => (
            <ListItem key={item} onClick={() => onClick(item.toLowerCase())}>
              <ListItemText
                className={
                  currentPage === `/${item.toLowerCase()}`
                    ? styles.selectedMenuItem
                    : styles.menuItem
                }
                primary={item}
              />
            </ListItem>
          ))}
        </List>
      </SwipeableDrawer>
    </>
  );
};

export default SidebarHeader;
