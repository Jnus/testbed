import React, { useState } from 'react';
import { connect } from 'react-redux';

import SidebarHeader from './sidebar-header';
import FullHeader from './full-header';
import { useHistory, useLocation } from 'react-router-dom';

interface IHeaderContainerProps {
  isPortrait: boolean;
  isMobile: boolean;
}

const Header = (props: IHeaderContainerProps) => {
  let location = useLocation();
  let history = useHistory();
  const [selectedPage, setSelectedPage] = useState(location.pathname);

  const menu = ['Travel', 'Food', 'Bio'];

  const changePage = (page: string) => {
    setSelectedPage(`/${page}`);
    history.push(page);
  };

  return (
    <>
      {props.isPortrait ? (
        <SidebarHeader
          currentPage={selectedPage}
          changePage={changePage}
          menu={menu}
          name={'JANUS KIONG'}
        />
      ) : (
        <FullHeader
          currentPage={selectedPage}
          changePage={changePage}
          menu={menu}
          name={'JANUS KIONG'}
        />
      )}
    </>
  );
};

const mapStateToProps = (state: any) => ({
  isMobile: state.config.isMobile,
  isPortrait: state.config.isPortrait,
});

export default connect(mapStateToProps, {})(Header);
