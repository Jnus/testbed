import React from 'react';

import styles from './header.module.css';
import { IHeaderProps } from './header-type';

const FullHeader: React.FC<IHeaderProps> = ({
  currentPage,
  changePage,
  menu,
  name,
}) => {
  return (
    <div className={styles.fullHeader}>
      <div className={styles.menuContainer} />
      <div className={styles.headerName} onClick={() => changePage('')}>
        {name}
      </div>
      <div className={styles.menuContainer}>
        {menu.map((item: string) => (
          <div
            className={
              currentPage === `/${item.toLowerCase()}`
                ? styles.selectedMenuItem
                : styles.menuItem
            }
            key={item}
            onClick={() => changePage(`${item.toLowerCase()}`)}
          >
            {item}
          </div>
        ))}
      </div>
    </div>
  );
};

export default FullHeader;
