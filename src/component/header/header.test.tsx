import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import FullHeader from './full-header';
import SidebarHeader from './sidebar-header';

test('Full Header: Clicking on item should set page', () => {
  const mockChangePage = jest.fn();
  render(
    <FullHeader
      currentPage={'/test'}
      changePage={mockChangePage}
      menu={['Travel', 'Test']}
      name={'Header'}
    />
  );

  fireEvent.click(screen.getByText('Travel'));

  expect(mockChangePage).toBeCalledWith('travel');

  fireEvent.click(screen.getByText('Header'));

  expect(mockChangePage).toBeCalledWith('');
});

test('Sidebar Header: Clicking on icon should open sidebar', () => {
  const mockChangePage = jest.fn();
  render(
    <SidebarHeader
      currentPage={'/test'}
      changePage={mockChangePage}
      menu={['Travel', 'Test']}
      name={'Sidebar'}
    />
  );

  fireEvent.click(screen.getByTestId('sidebar-button'));

  fireEvent.click(screen.getByText('Travel'));

  expect(mockChangePage).toBeCalledWith('travel');
});
