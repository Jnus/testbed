export interface IHeaderProps {
  currentPage: string;
  changePage: (page: string) => void;
  menu: string[];
  name: string;
}
