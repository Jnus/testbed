import React, { useState } from 'react';
import SousVide from './sous-vide/sous-vide';
import { FormControl, InputLabel, MenuItem, Select } from '@material-ui/core';

const Food = () => {
  const [groupBy, setGroupBy] = useState('course');

  const changeGroupBy = (event: React.ChangeEvent<{ value: unknown }>) => {
    setGroupBy(event.target.value as string);
  };

  return (
    <>
      <div>Food</div>
      <div>Recipes</div>
      <FormControl variant="outlined">
        <InputLabel>Group By</InputLabel>
        <Select value={groupBy} onChange={changeGroupBy} label="">
          <MenuItem value={'course'}>Course</MenuItem>
          <MenuItem value={'method'}>Method</MenuItem>
        </Select>
      </FormControl>
      {groupBy === 'course' && <div>Carousel of random dishes</div>}
      {groupBy === 'method' && (
        <>
          <div>Boil</div>
          <div>Bake</div>
          <SousVide />
        </>
      )}
    </>
  );
};

export default Food;
