import axios from "axios";
import { getRecipe } from "./food-api";

jest.mock('axios');

describe('Food API', () => {
  test('fetch data successfully', async () => {
    const data = {
      data: [
        {
          description: ['Test'],
          ingredients: [{'name': 'test', quantity: 1, unit: 'g'}],
          steps: ['Test step']
        },
      ],
    };

    // @ts-ignore
    axios.get.mockImplementationOnce(() => Promise.resolve(data));

    expect(await getRecipe('Test')).toEqual(data.data);

    expect(axios.get).toHaveBeenCalledWith(
      'https://raw.githubusercontent.com/JnusK/content/main/recipe/test.json'
    );
  });

  test('Error should be thrown', async () => {
    const errorMessage = 'Request failed with status code 404';
    // @ts-ignore
    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage))
    );

    await expect(getRecipe('Test')).rejects.toThrow(errorMessage);
  });
});
