import axios from 'axios';

export const getRecipe = async (dish: string) => {
  try {
    const res = await axios.get(
      `https://raw.githubusercontent.com/JnusK/content/main/recipe/${dish.toLowerCase()}.json`
    );
    return res.data;
  } catch (error) {
    throw error;
  }
};