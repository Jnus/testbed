import React from 'react';
import { Legend, Scatter, ScatterChart, Tooltip, XAxis, YAxis } from 'recharts';

import styles from './sous-vide.module.css';

const sousVideData = {
  beef: {
    data: [
      {
        cut: 'Ribeye Steak',
        temp: 54,
        time: 2,
      },
    ],
    color: '#8884d8',
  },
  pork: {
    data: [
      {
        cut: 'Shoulder Butt',
        temp: 64,
        time: 2,
      },
    ],
    color: '#82ca9d',
  },
  chicken: {
    data: [
      {
        cut: 'Breast',
        temp: 60,
        time: 1.5,
      },
      {
        cut: 'Thigh',
        temp: 64,
        time: 1.5,
      },
    ],
    color: '#ffc8dd',
  },
};

interface ICustomTooltip {
  active: boolean;
  payload: any[];
  label: string;
}

const CustomTooltip: React.FC<ICustomTooltip> = ({
  active,
  payload,
  label,
}) => {
  if (active && payload && payload.length) {
    return (
      <div className={styles.tooltip}>
        <p>{payload[0].payload.cut}</p>
        {payload.map((line, index: number) => (
          <p key={index}>{`${line.name} : ${line.value}${line.unit}`}</p>
        ))}
      </div>
    );
  }
  return null;
};

const SousVideChart = () => {
  return (
    <ScatterChart width={720} height={480}>
      <XAxis dataKey="time" name="Duration" unit="H" />
      <YAxis
        dataKey="temp"
        domain={['dataMin - 5', 'dataMax + 5']}
        name="Temperature"
        unit="°C"
      />
      {/*<ZAxis dataKey={} name={} range={[]} unit={} />*/}
      <Tooltip
        cursor={{ strokeDasharray: '3 3' }}
        content={
          // @ts-ignore
          <CustomTooltip />
        }
      />
      <Legend />
      {Object.entries(sousVideData).map(([key, value], index: number) => (
        <Scatter name={key} data={value.data} fill={value.color} key={index} />
      ))}
    </ScatterChart>
  );
};

export default SousVideChart;
