import React from 'react';
import SousVideChart from './sous-vide-chart';

const SousVide = () => {
  return (
    <>
      <div>Sous Vide Skeleton</div>
      <SousVideChart />
    </>
  );
};

export default SousVide;
