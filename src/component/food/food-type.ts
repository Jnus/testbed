export interface IRecipe {
  description: string[];
  ingredients: IIngredient[];
  steps: string[];
}

export interface IIngredient {
  name: string;
  quantity: number;
  unit: 'g' | 'Kg' | 'ml' | 'tbsp' | 'tsp' | 'cup' | 'to taste' | 'pcs' | 'small' | 'medium' | 'large';
}
