import React from 'react';
import RecipeDescription from './recipe-description';
import RecipeInstructions from './recipe-instructions';

import styles from './recipe.module.css';

const Recipe = () => {
  return (
    <div className={styles.recipeContainer}>
      <RecipeDescription />
      <RecipeInstructions
        recipe={{
          steps: [],
          ingredients: [{ name: '', quantity: 1, unit: 'g' }],
        }}
      />
    </div>
  );
};

export default Recipe;
