import React from 'react';
import { IIngredient, IRecipe } from '../food-type';

import styles from '../recipe.module.css';

interface IRecipeProps {
  recipe: IRecipe;
}

const RecipeInstructions: React.FC<IRecipeProps> = ({ recipe }) => {
  return (
    <div className={styles.instructionsContainer}>
      <ul>
        {recipe.ingredients.map((ingredient: IIngredient, index: number) => {
          return (
            <li
              key={index}
            >{`${ingredient.quantity} ${ingredient.unit} ${ingredient.name}`}</li>
          );
        })}
      </ul>
      <ol>
        {recipe.steps.map((step: string, index: number) => (
          <li key={index}>{step}</li>
        ))}
      </ol>
    </div>
  );
};

export default RecipeInstructions;
