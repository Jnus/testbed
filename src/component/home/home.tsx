import React from 'react';
import TravelSummary from './travel-summary';
import RecipeSummary from './recipe-summary';
import utahRoad from '../../static/img/utah-road.jpg';
import portrait from '../../static/img/denali-portrait-cr.jpg';

import styles from './home.module.css';

const Home = () => {
  return (
    <div>
      <div className={styles.landingContainer}>
        <img alt="Straight Road" className={styles.landingImg} src={utahRoad} />
        <div className={styles.landingText}>
          <div>Experiment</div>
          <div>Experience</div>
          <div>Enjoy</div>
        </div>
      </div>
      <div className={styles.bylineContainer}>
        <img alt="Janus" className={styles.portrait} src={portrait} />
        <div className={styles.byline}>
          Janus is a software developer by trade. This is a website to chronicle
          his leisure escapades.
        </div>
      </div>
      <TravelSummary />
      <RecipeSummary />
    </div>
  );
};

export default Home;
