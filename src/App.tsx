import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from './component/home/home';
import Header from './component/header/header';
import CheckBrowserWidth from './component/config/CheckBrowserWidth';
import Footer from './component/footer/footer';
import Load from './component/load/load';

import styles from './App.module.css';

const Travel = lazy(() => import('./component/travel/travel'));
const Bio = lazy(() => import('./component/bio/bio'));
const Food = lazy(() => import('./component/food/food'));

const App = () => {
  return (
    <Router>
      <div className={styles.App}>
        <CheckBrowserWidth />
        <Header />
        <Suspense fallback={<Load />}>
          <Switch>
            <Route path="/bio">
              <Bio />
            </Route>
            <Route path="/travel">
              <Travel />
            </Route>
            <Route path="/food">
              <Food />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Suspense>

        <Footer />
      </div>
    </Router>
  );
};

export default App;
